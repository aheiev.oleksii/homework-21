let amount = 100;

let monday = [
  ["Write a tutorial", 180],
  ["Some web development", 120],
];

let tuesday = [
  ["Keep writing that tutorial", 240],
  ["Some more web development", 360],
  ["A whole lot of nothing", 240],
];

function convertTime(arr) {
  const newArr = arr
    .map(function (el) {
      el[1] = el[1] / 60;
      return el;
    });
  
  return newArr;
}

function filterTask(arr) {
  const filtredArr = arr
    .filter(function (el) {
      return el[1] > 2;
    });
  
  return filtredArr;
}

function multAndAddToArr(arr) {
  const newMainArr = arr.map(function (el) {
    el.push(el[1] * amount);
    return el;
  });

  return newMainArr;
}

function printTable(arr) {
  const print = arr.map(function (el) {
    return `
      <tr>
        <td>Task name: ${el[0]}</td>
        <td>Taks duration: ${el[1]} hours</td>
        <td>Task amount: $${el[2]}</td>
      </tr>`;
  });

  return print;
}

const printMonday = printTable(multAndAddToArr(filterTask(convertTime(monday))));
const printTuesday = printTable(multAndAddToArr(filterTask(convertTime(tuesday))));

document.write(`
  <table>
    <tbody>
      ${printMonday}
      ${printTuesday}
    </tbody>
  </table>
`);
